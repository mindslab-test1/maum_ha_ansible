written by mjchae

# ansible automation for maum_ha
## ansible for LB(haproxy + keepalived)

MAUM_HA (Haproxy, keealpived) 의 설정파일들을 ansible을 통해 자동으로 복사 및 설정

CONFIG.md 에는 haproxy와 keepalived 의 설명이 있음.

***
### how to clone repo including submodules
```
git clone --recurse-submodules https://pms.maum.ai/bitbucket/scm/aiccs/maum_ha_ansible.git
```

haproxy 2.3 및 keepalived 함께 clone

***

### LB install, configure
- install rpm packages



```
cd RPM(offline 패키지 설치)
rpm ansible-2.9.15-1.el7.noarch.rpm

cd RPM/HAProxy(offline 패키지 설치)
rpm -Uvh --force --nodeps *.rpm

cd RPM/Keepalived(offline only)
rpm -Uvh --force --nodeps *.rpm
```

***
- install and configure Load Balancing package. HAProxy, Keepalived
```
ansible-playbook -vv LB_install_config.yaml
ansible-playbook -vv LB_install_config_offline.yaml (for offline)
```


***
### Setup HAProxy, Keepalived
```
ansible-playbook -vv LB_setup.yaml -e "state=[] interface=[] src_ip=[] peer_ip=[] vip=[] port=[]"

ex) ansible-playbook -vv LB_setup.yaml -e "state=BACKUP interface=enp5s0 src_ip=10.122.64.95 peer_ip=10.122.64.191 vip=10.122.64.119/24 port=50051"
```

state : keeaplaived 의 master, backup 결정(MASTER or BACKUP)

interface : 현재 장치의 network interface (ifconfig 등으로 확인)

src_ip : 현재 장치의 IP 주소 입력

peer_ip : load balancing 할 서버의 IP 주소 입력

vip : bind 시킬 virtual IP 입력

port : bind 된 virtual IP의 port 입력
