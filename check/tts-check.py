#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import grpc
import getopt
import ng_tts_pb2
import ng_tts_pb2_grpc
from maum.common import lang_pb2

# For your reference
TTS = '127.0.0.1:50051'

def unittask():
    channel = grpc.insecure_channel(host)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    req.sampleRate = sample_rate
    req.text = tts_text
    req.speaker = 0
    print('TTS req.text: {}'.format(req.text.encode('utf-8')))

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        f = open('/srv/maum/check/test00.wav', 'w')
        for tts in resp:
            f.write(tts.mediaData)
        f.close()
    except grpc.RpcError as e:
        print >> sys.stderr, str('SpeakWav() failed with {0}: {1}'.format(e.code(), e.details()))
        f.close()
        sys.exit(2)


if __name__ == '__main__':
    host = TTS
    sample_rate = 8000
    tts_text = '체크'

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'h:', [])
        for option, value in opts:
            if option == '-h':
                host = value

        if len(args) < 1:
            sys.exit(2)
        tts_text = args[0]

    except getopt.GetoptError as err:
        print >> sys.stderr, str(err)  
        sys.exit(2)

    start = time.time()
    unittask()

    total = (time.time() - start) * 1000
    print('TTS task total time: {:.2f} ms'.format(total))
    print('TTS Result=OK')
    sys.exit(0)

