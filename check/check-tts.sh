#!/bin/bash
export PYTHONPATH=/srv/maum/lib/python
PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/srv/maum/bin
VIP=$1
VPT=$2
RIP=$3
# check if $4 empty (Ie. a Multport VIP) 
if [ "$4" == "" ]; then   
    # We are multiport - use the check port or VIP first port
    RPT=$VPT  
else 
    RPT=$4
fi

RESULT_STRING="OK"

# Build options variable
TTS_TEXT="안녕!"
CHECK_OPT="-h ${RIP}:${RPT}"

# Run curl with appropriate options
#python /srv/maum/tts-check.py ${CHECK_OPT} ${TTS_TEXT} 2>/dev/null | grep -q "${RESULT_STRING}"
/usr/bin/python /srv/maum/check/tts-check.py ${CHECK_OPT} ${TTS_TEXT} 2>/dev/null 
exit $?
